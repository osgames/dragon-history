block _22 ( atbegin(1)  and not been(_22) )
   title Przykro mi, ale nie mog� ci� st�d zabra�
  {extra pokecat muzeme jenom jednou, takze to hned tady znemoznime:}
  let kronika_extra_pokec (0)

  justtalk
   D: "Dzie� dobry, kroniko."
   D: "Przykro mi, ale nie mog� ci� st�d zabra�."
  juststay

  start kronika "mluvi"
   K: "No to id�, ja tu zostan�."
  start kronika "mlci"

  justtalk
   D: "Zostaniesz?
       Gobliny zbudz� si�, a wtedy..."
  juststay

  start kronika "mluvi"
   K: "Czekam na t� chwil�,
       Chc� zrobi� im awantur�!"
  start kronika "mlci"

  justtalk
   D: "To dobry pomys�, przypuszczam, �e
       ju� teraz, �pi�c trz�s� si� ze strachu!"
  juststay

  start kronika "mluvi"
   K: "Tak s�dzisz?"
  start kronika "mlci"

  justtalk
   D: "Tak, naprawd� tak my�l�."
  juststay

  start kronika "mluvi"
   K: "Powiedz mi szczerze,
       czy si� mnie boisz?"
  start kronika "mlci"

  justtalk
   D: "Tak, a� zsiusia�em si� w pieluszki!"
  juststay

{  Vypravec:"[Obstaro�n� kronika bal� pubert�ln�ho draka:]"
  start kronika "mluvi"
   K: "Oh!"
   K: "To by� tylko komplement
       od przystojnego m�odzie�ca, prawda?"
  start kronika "mlci"

  justtalk
   D: "Jasne, ju� od sze�ciu miesi�cy nie siusiam
       w pieluszki..."
  juststay

  start kronika "mluvi"
   K: "Och, taki du�y ch�opiec!
       A w jaki spos�b robisz siusiu?"
  start kronika "mlci"

  justtalk
   D: "Spokojnie.
       Nie bierz sobie tego tak do serca, dobrze?"
  juststay

  justtalk
   D: "Je�li powiedzia�bym ci komplement,
       to chyba przez pomylk�!"
  juststay

  start kronika "mluvi"
   K: "Zgoda, spodziewa�am si�, �e
       mo�emy sobie troch� po�artowa�."
  start kronika "mlci"

   Vypravec: "[Niemi�y go��:]"
  justtalk
   D: "Nie ma czasu na takie rzeczy!"
  juststay

  justtalk
   D: "S�uchaj, je�li si� tob� zajmuj� to tylko
       z naukowego i historycznego punktu widzenia!"
  juststay

  justtalk
   D: "Kiedy sobie przypominam, co oni z tob� zrobili..."
  juststay

  start kronika "mluvi"
   K: "Nie martw si�. Jestem wykonana z
       niepalnego materia�u!"
  start kronika "mlci"

  justtalk
   D: "OK, lecz co b�dzie, jak oni..."
   D: "Co wytrzyma materia�, z kt�rego
       jeste� zrobiona?"
  juststay

  start kronika "mluvi"
   K: "Jest odporny na mr�z, gor�co, wodoszczelny
       przetrwa up�yw czasu i jest niejadalny."
  start kronika "mlci"

  justtalk
   D: "Czy kto� pr�buje zjada� stare kroniki?
       Fe!"
  juststay

  start kronika "mluvi"
   K: "W bajkach wszystko jest mo�liwe..."
  start kronika "mlci"

  justtalk
   D: "Ciesz� si�, z� mog� bez obaw
       zostawi� ci� tu na pastw� tych idiot�w."
   D: "Narazie."
   D: "By� mo�e spotkamy si� p��niej ..."
  juststay

gplend
