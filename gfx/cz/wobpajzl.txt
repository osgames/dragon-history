{s pajzlem u obra:}

block _00a ( atbegin(1) and not hasbeen(_00a) )
title
     justtalk
     D: "Ehm, pane trpasl�ku..."
     D: "Dovol�m si v�m polo�it n�kolik ot�zek.
         Nebudu v�s moc zdr�ovat,
         vid�m, �e toho m�te na pr�ci dost."
     juststay
     start obr_pajzl2 "mluvi"
     Pajzl: "Hmptf, ptej se!"
     start obr_pajzl2 "mlci"
gplend

block _00b ( atbegin(1) and hasbeen(_00a) )
title
     justtalk
     D: "Omylem jsem spolknul v�echny svoje pozn�mky.
         Sjedeme to je�t� jednou?"
     juststay

     start obr_pajzl2 "mluvi"
     Pajzl: "Urgh!"
     start obr_pajzl2 "mlci"

     justtalk
     D: "�e ne!?"
     D: "Mezi n�ma, ty pozn�mky byly p�knej hnus."
     D: "Pokuste se znova koncentrovat..."
     juststay
     D: "..."
     justtalk
     D: "Jdem na to?"
     juststay
     start obr_pajzl2 "mluvi"
     Pajzl: "No dobr�."
     start obr_pajzl2 "mlci"
gplend

block _01 ( atbegin(0) )
title Jak se c�t�te?
     justtalk
     D: "Jak se c�t�te?"
     juststay
     start obr_pajzl2 "mluvi"
     Pajzl: "Jsem v naprost� pohod�!"
     start obr_pajzl2 "mlci"
     justtalk
     D: "Kolik jste toho natr�noval?"
     juststay
     start obr_pajzl2 "mluvi"
     Pajzl: "Hmptf!"
     start obr_pajzl2 "mlci"
     justtalk
     D: "V���te si?"
     juststay
     start obr_pajzl2 "mluvi"
     Pajzl: "Absolutn�!"
     start obr_pajzl2 "mlci"
     justtalk
     D: "Mysl�te si, �e jste ve form�?"
     juststay
     start obr_pajzl2 "mluvi"
     Pajzl: "Formu m�m na�asovanou spr�vn�!"
     start obr_pajzl2 "mlci"
     justtalk
     D: "Nep�etr�noval jste?"
     juststay
     start obr_pajzl2 "mluvi"
     Pajzl: "Nesmysl."
     start obr_pajzl2 "mlci"
     justtalk
     D: "Pot�te se?"
     juststay
     start obr_pajzl2 "mluvi"
     Pajzl: "Norm�ln�."
     start obr_pajzl2 "mlci"
     justtalk
     D: "Co nervozita?"
     juststay
     start obr_pajzl2 "mluvi"
     Pajzl: "Jsem v klidu."
     start obr_pajzl2 "mlci"
     justtalk
     D: "Netla�� v�s moc ten �pag�t?"
     juststay
     start obr_pajzl2 "mluvi"
     Pajzl: "D�ky, je to dobr�."
     start obr_pajzl2 "mlci"
     justtalk
     D: "Ani ten �pag�t moc neza�ez�v�?"
     juststay
     start obr_pajzl2 "mluvi"
     Pajzl: "Fakt dobr�."
     start obr_pajzl2 "mlci"
     justtalk
     D: "Co ��k�te na to, �e budete
         pravd�podobn� sn�zen?"
     juststay
     start obr_pajzl2 "mluvi"
     Pajzl: "Douf�m, �e m� p��li� nep�epep��.
             Trp�m sennou r�mou."
     start obr_pajzl2 "mlci"
     justtalk
     D: "� vida, tak�e p�ece jenom mal� handicap..."
     juststay
     start obr_pajzl2 "mluvi"
     Pajzl: "To tvrd�te vy. J� jsem absolutn� O.K."
     start obr_pajzl2 "mlci"
     justtalk
     D: "Jak se tedy c�t�te t�sn� p�ed svou smrt�?"
     juststay
     start obr_pajzl2 "mluvi"
     Pajzl: "Je to pro m� zcela nov� �ivotn� zku�enost."
     start obr_pajzl2 "mlci"
     justtalk
     D: "D�ky za rozhovor!"
     juststay
     start obr_pajzl2 "mluvi"
     Pajzl: "R�do se stalo."
     Pajzl: "Publicita je hrozn� d�le�it�."
     start obr_pajzl2 "mlci"
gplend
