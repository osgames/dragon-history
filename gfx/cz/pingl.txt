{rozhovor s pinglem v hospod�:}

block _05 ( beg(1) and isactico(i_doutnik) and (chci_plakat=1) )
title
  justtalk
     D: "Co takhle doutn��ek?"
  juststay
  start pingl "mluvi"
  Vrch: "L�kav�..."
  start pingl "mlci"
  justtalk
     D: "Origin�l z Dra�� Historie."
  juststay
  start pingl "mluvi"
  Vrch: "Dej to sem!"
  start pingl "mlci"
  justtalk
     D: "Vykou��te ho hned?"
  juststay
  start pingl "mluvi"
  Vrch: "Co�pak nev��,
         �e kou�en� �kod� zdrav�?!"
  Vrch: "Kdepak, takovou vz�cnost
         budu jenom pe�liv� opatrovat."
  Vrch: "Mo�n� se na n�j jednou za �as pod�v�m..."
  Vrch: "...a ten plak�t je samoz�ejm� tv�j!"
  start pingl "mlci"

  objstat away hosp_plakat
  icostat off i_doutnik
  icostat on i_plakat
  exitdialogue
gplend

block _00 ( beg(1) and maxline(1) )
title
  justtalk
     D: "Pane hostinsk�..."
  juststay
gplend

block _04 ( beg(0) and not been(_04) )
title ...co ten plak�t na st�n�?
  justtalk
     D: "...co ten plak�t na st�n�?"
  juststay
  start pingl "mluvi"
  Vrch: "L�b� se ti?"
  start pingl "mlci"
  justtalk
     D: "R�d bych ho m�l doma nad postel�."
  juststay
  start pingl "mluvi"
  Vrch: "A co bys je�t� cht�l?"
  start pingl "mlci"
  justtalk
     D: "Odzn��ky, n�lepky, vlaje�ky..."
  juststay
  justtalk
     D: "...pivn� t�cek, nem�te? ..."
  juststay
  justtalk
     D: "...taky k�iltovku..."
  juststay
  justtalk
     D: "...a tri�ko s n�pisem!"
  juststay
  start pingl "mluvi"
  Vrch: "Nic z toho nem�m!"
  start pingl "mlci"
  justtalk
     D: "A co ten plak�t?"
  juststay
  start pingl "mluvi"
  Vrch: "Ten bych ti mo�n� mohl v�novat,
         ale zadarmo to nebude."
  start pingl "mlci"

  let chci_plakat (1)
gplend

block _02 ( beg(0) and not been(_02) )
title ...p�j��te mi �ipky?
  justtalk
     D: "...p�j��te mi �ipky?"
  juststay
  start pingl "mluvi"
  Vrch: "�ipky nep�j�ujeme."
  start pingl "mlci"
  justtalk
     D: "Pro�pak?"
  juststay
  start pingl "mluvi"
  Vrch: "��dn� �ipky nejsou."
  start pingl "mlci"
  justtalk
     D: "To nech�pu!"
  juststay
  start pingl "mluvi"
  Vrch: "Prost� nejsou."
  Vrch: "Kdyby byly,
         bylo by to tu mo�n� pln�j��..."
  Vrch: "Dokonce jeden z na�ich �ast�ch host�
         se �el p�ed chv�l� kv�li �ipk�m ob�sit do lesa."
  start pingl "mlci"
  justtalk
     D: "Kam p�esn�?"
  juststay
  start pingl "mluvi"
  Vrch: "Nev�m, �el si teprve vyhl�dnout v�tev..."
  start pingl "mlci"

{a objevime sipkare v lese:}
  objstat_on leskriz_sipkar leskriz
gplend

block _03 ( beg(0) and not been(_03) )
title ...kam vedou ty dve�e?
  justtalk
     D: "...kam vedou ty dve�e?"
  juststay
  start pingl "mluvi"
  Vrch: "P�ed hospodu. Ale nepou��vaj� se kv�li �ipk�m."
  start pingl "mlci"
  justtalk
     D: "To bude mo�n� zaj�mav� story..."
  juststay
  start pingl "mluvi"
  Vrch: "P�vodn� se dve�mi vedle ter�e opravdu chodilo,
         ale st�vala se spousta nehod."
  Vrch: "Kdy� n�kdo vstoupil ne�ekan� dovnit�,
         �asto se p�ihodilo,
         �e se mu �ipka zabodla do obli�eje."
  start pingl "mlci"
  justtalk
     D: "Jak jste to potom po��tali?"
  juststay
  start pingl "mluvi"
  Vrch: "Oko za deset bod� a nos za p�t."
  Vrch: "Jestli�e byl rozmach obzvl��� siln�,
         dva body za ka�d� vyra�en� zub."
  Vrch: "A pokud �ipka let�la tak prudce,
         �e s sebou toho �lov�ka nabrala..."
  Vrch: "...a p�ip�chla ho p�ed hospodou
         na strom, tak za sto."
  start pingl "mlci"
  justtalk
     D: "Je to podle pravidel?"
  juststay
  start pingl "mluvi"
  Vrch: "No to je pr�v� ono, bylo to dost sporn�."
  Vrch: "A proto se dve�e pro jistotu nav�dy zam�ely
         a kl�� byl bezpe�n� ztracen."
  start pingl "mlci"
gplend

block _01 ( beg(0) and not been(_01) )
title ...m�te utopence?
  justtalk
     D: "...m�te utopence?"
  juststay
  start pingl "mluvi"
  Vrch: "Neva��me!"
  start pingl "mlci"
  justtalk
     D: "Nemus� b�t va�enej..."
  juststay
  start pingl "mluvi"
  Vrch: "Nem�me kuchyni."
  start pingl "mlci"
  justtalk
     D: "A co ten n�pis venku?"
  juststay
  start pingl "mluvi"
  Vrch: "Reklamn� trik."
  start pingl "mlci"
  justtalk
     D: "Aha."
  juststay
gplend



block _06 ( beg(0) and been(_04) and not been(_05) and last(_00) and maxline(1) )
title
  justtalk
     D: "...ten plak�t..."
  juststay
  start pingl "mluvi"
  Vrch: "Myslel jsem, �e si rozum�me.
         M��e b�t tv�j, ale ne zadarmo."
  start pingl "mlci"
  exitdialogue
gplend

block _08 ( beg(0) and last(_00) and maxline(1) )
title
  justtalk
     D: "...vlastn� nic."
  juststay
  exitdialogue
gplend

block _07 ( beg(0) and not last(_00) )
title ...tak zat�m.
  justtalk
     D: "...tak zat�m."
  juststay
  start pingl "mluvi"
  Vrch: "Jo..."
  start pingl "mlci"
  exitdialogue
gplend
