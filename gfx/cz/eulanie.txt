{dialog s Eulanii- ufnukanou zidli:}

block _00 ( beg(1) )
title
  justtalk
  D: "Pan� stoli�ko..."
  D: "Mil� kuchy�sk� �tokrl�tko..."
  D: "Vzne�en� �idli�ko!"
  D: "Urozen� k�es�lko..."
  juststay
  start zid_prava_huba "mluvi"
{#this part is based on the large variety of words
{#for a very little (arm-)chair in Czech
  Z2: "Ach, p�eje� si snad n�co?"
  start zid_prava_huba "mlci"
  justtalk
  D: "Hled�m n�koho,
      s k�m bych si mohl trochu u��t
      sv� extrovertn� povahy."
  juststay
  justtalk
  D: "Chci si s v�mi pov�dat!"
  juststay
  start zid_prava_huba "mluvi"
  Z2: "Ale j� jsem introvertn� �idle..."
  start zid_prava_huba "mlci"
  justtalk
  D: "Nevad�, budu pov�dat hlavn� j�."
  juststay
  start zid_prava_huba "mluvi"
  Z2: "Obt��uje� m�."
  start zid_prava_huba "mlci"
  justtalk
  D: "Copak �e jsi takov� do sebe uzav�en�?
      Nechci t� rozzlobit..."
  juststay
  start zid_prava_huba "mluvi"
  Z2: "Ne, rozzlobit, to snad ne..."
  start zid_prava_huba "mlci"
  justtalk
  D: "Vypad�� tak p�kn� a sympaticky!"
  juststay
  start zid_prava_huba "mluvi"
  Z2: "Mysl��?"
  start zid_prava_huba "mlci"
  justtalk
  D: "V��n�.
      �estn� slovo."
  juststay
  start zid_prava_huba "mluvi"
  Z2: "L�e� mi."
  start zid_prava_huba "mlci"
  justtalk
  D: "Kdepak by se vzalo.
      Mluv�m pravdu jako kdy� tiskne."
{#this is based on the the phrase "to tell downright lies"
{#the sentence "to tell downright the truth" is imposiible
{#and it sounds ridiculous in Czech
  juststay
  start zid_prava_huba "mluvi"
  Z2: "L�e� a chce� m� zesm��nit.
       V�bec se ti nel�b�m."
  start zid_prava_huba "mlci"
gplend

block _01 ( last(_00) )
title
  justtalk
  D: "Co to pov�d��?"
  juststay
  start zid_prava_huba "mluvi"
  Z2: "Nel�b�m se toti� nikomu!"
  start zid_prava_huba "mlci"
  justtalk
  D: "Jak jsi mohla na takov� nesmysl p�ij�t?"
  D: "M� se l�b��,
      hned bych t� cht�l m�t doma na p�d�!"
  juststay
  start zid_prava_huba "mluvi"
  Z2: "M�m strach z v��ek..."
  start zid_prava_huba "mlci"
  justtalk
  D: "Tak v komo�e!"
  juststay
  start zid_prava_huba "mluvi"
  Z2: "Nesn���m �p�nu."
  start zid_prava_huba "mlci"
  justtalk
  D: "Tak potom nev�m, kam s tebou.
      Ob�v�k si od tebe hyzdit nenech�m."
  juststay
  start zid_prava_huba "mluvi"
  Z2: "Vid��, te� ses projevil."
  start zid_prava_huba "mlci"
  justtalk
  D: "Jenom jsem �ertoval."
  D: "Cht�l jsem ti uk�zat,
      jak� velk� nesmysly pov�d��.
      Jsi v��n� �ik."
  juststay
  start zid_prava_huba "mluvi"
  Z2: "Jenom�e to nen� pravda.
       Vid�� tu malou te�ku na m� prav� p�edn� noze?"
  start zid_prava_huba "mlci"
  justtalk
  D: "Kterou?"
  juststay
  start zid_prava_huba "mluvi"
  Z2: "Je to takov� mal� �u�ka p��mo uprost�ed."
  {p��mo pla�tiv�:}
  Z2: "A je HROZN� vid�t!"
  start zid_prava_huba "mlci"
  {drak zabrejl�:}
  justtalk
  D: "Nic nevid�m..."
  juststay
  start zid_prava_huba "mluvi"
  Z2: "...od �ervoto�e!"
  start zid_prava_huba "mlci"
  justtalk
  D: "Jo tahle!"
  D: "Ale je docela mal�..."
  juststay
  start zid_prava_huba "mluvi"
  Z2: "No vid��, nikomu se nel�b�m,
       v�ichni m� cht�j� jenom zesm��nit,
       tak se nediv, �e jsem potom introvertn�."
  start zid_prava_huba "mlci"
  justtalk
  D: "To potom opravdu nen� divu."
  D: "(S n� asi moc �e�� nebude,
      rad�ji se poohl�dnu je�t� n�kde jinde.)"
  juststay
gplend
