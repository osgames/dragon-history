{s pajzlem v hospod�:}
block _00 ( beg(1) and not been(_00) )
title
  justtalk
D: "Herr Zwerg..."
D: "W�rdest du mir ein Bier ausgeben?"
  juststay
  start hpajzl_hlava "mluvi"
P: "Verschwinde."
  start hpajzl_hlava "mlci"
  justtalk
D: "Ich lass mich nicht so leicht einsch�chtern!"
  juststay
  start hpajzl_hlava "mluvi"
P: "Das Bier ist warm!"
  start hpajzl_hlava "mlci"
  justtalk
D: "Um Gottes Willen, wieviel hast du denn gehabt?"
  juststay
  start hpajzl_hlava "mluvi"
P: "Verschwinde, Junge!"
  start hpajzl_hlava "pije"
  justtalk
D: "Du bist nicht sehr gespr�chig...
    Du siehst ersch�pft aus."
D: "Hast du ein schreckliches Trauma erlebt?"
  juststay
  start hpajzl_hlava "mluvi"
P: "Geht dich nichts an!"
  start hpajzl_hlava "mlci"
  justtalk
D: "Du mu�t alles rauslassen..."
  juststay
  start hpajzl_hlava "mluvi"
P: "Na sch�n,
    wo ist hier das Klo?"
  start hpajzl_hlava "mlci"
  justtalk
D: "...all die f�rchterlichen Erlebnisse,
    die sich in dir auft�rmen!"
  juststay
gplend

block _00b ( last(_00) )
title
 labels skip
  start hpajzl_hlava "mluvi"
P: "Bier!"
  start hpajzl_hlava "pije"
  justtalk
D: "Versuch doch nicht all deine Probleme
    in Alkohol zu ertr�nken!"
  juststay
  start hpajzl_hlava "mluvi"
P: "Was? Warum denn nicht?"
  goto skip
start hpajzl_hlava "mlci"
justtalk
D: "Daf�r gibt's doch Medikamente,
    die man missbrauchen kann."
juststay
start hpajzl_hlava "mluvi"
P: "Wirklich?
    Das sollte ich mal versuchen."
 label skip
P: "Nun verstehe ich.
    Du hast mir die Augen ge�ffnet."
  start hpajzl_hlava "mlci"
  justtalk
D: "Vertraust du dich mir jetzt an?"
  juststay
  start hpajzl_hlava "mluvi"
P: "Ja,
    Vielleicht w�re es wirklich besser,
    wenn ich mich dir anvertraue."
P: "Ich bin nur knapp
    dem Rachen des Todes entkommen!"
P: "Ich wurde fast verspeist."
P: "Das war's."
  start hpajzl_hlava "mlci"
  justtalk
D: "Na also!"
  juststay
  start hpajzl_hlava "mluvi"
P: "Das ist ja schrecklich,
    wie kann es sich jemand heutzutage leisten,
    Zwerge zu essen."
P: "Ja, es gibt L�nder,
    in denen es bereits seit vielen Jahren
    verboten ist, Zwerge zu essen."
  start hpajzl_hlava "mlci"
gplend

block _00c ( last(_00b) )
title
 labels skip
  start hpajzl_hlava "mluvi"
P: "Sind wir etwa nicht zivilisiert genug, Junge?"
  start hpajzl_hlava "mlci"
  justtalk
D: "�hm, das ist ein weltweiter Skandal."
  juststay
  start hpajzl_hlava "mluvi"
P: "Wenigstens stimmst du mir zu."
P: "Aber was soll ich nur tun,
    um meine armen Br�der davor zu bewahren,
    was sie erwartet."
  goto skip
P: "Ja, was soll ich tun."
  start hpajzl_hlava "mlci"
  justtalk
D: "Erwartet sie ein sehr traumatisches Erlebnis?"
  juststay
  start hpajzl_hlava "mluvi"
P: "Ja, das tut es,
    und erst dieser Geruch."
  label skip
P: "Zuerst wird er sie pfeffern."
P: "Dann durchklopfen."
P: "Dann kochen."
P: "Vielleicht frittiert er sie sogar."
P: "...uff!..."
P: "Das w�re nichts f�r mich,
    ich vertrag nicht soviel Cholesterin!"
P: "Oder Brotkr�mel in den Augen."
P: "Dann gie�t er etwas Zitronensaft �ber sie..."
  start hpajzl_hlava "pije"
  justtalk
D: "[Mir l�uft das Wasser im Mund zusammen.
    Wo k�nnte ich wohl
    so ein exotisches Gericht bekommen?]"
D: "Laden sie mich zu einer Feier ein?"
  juststay
  {pajzl odpov�d� velice smutn� a zklaman�:}
  start hpajzl_hlava "mluvi"
P: "Ich lade dich nicht ein. Ich bin deprimiert."
  start hpajzl_hlava "pije"
gplend

block _00d ( last(_00c) )
title
  justtalk
D: "Tut mir leid,
    ich wollte nicht teilnahmslos erscheinen.
    Wo sind deine Br�der?"
D: "Ich werde sie befreien!"
  juststay
  start hpajzl_hlava "mluvi"
P: "Das w�rdest du tun?"
  start hpajzl_hlava "mlci"
  justtalk
D: "Na klar."
  juststay
  start hpajzl_hlava "mluvi"
P: "Meine Br�der warten darauf
    verspeist zu werden..."
P: "...in der furchtbaren H�hle
    eines noch furchtbareren Riesen."
  start hpajzl_hlava "mlci"
  justtalk
D: "Wo genau?"
  juststay
  start hpajzl_hlava "mluvi"
P: "Richtung Nord-Nord-Ost."
P: "Wenn du's genau wissen willst,
    es sind 515 Grad und 75 Winkelstunden
    ...ich meine, Minuten."
  start hpajzl_hlava "mlci"
  justtalk
D: "Das werd ich kaum finden!"
  juststay
  start hpajzl_hlava "mluvi"
P: "Und dazu 60 Winkelsekunden."
  start hpajzl_hlava "mlci"
  justtalk
D: "Das hilft mir schon eher!
    Jetzt wei� ich genau, wo sie ist."
D: "Ich mach mich auf!"
  juststay
  start hpajzl_hlava "mluvi"
P: "Viel Gl�ck.
    Sag meinen Br�dern,
    da� ich hier auf sie warten werde."
P: "Wenn sie mich nicht finden,
    sollen sie unterm Tisch nachsehen."
  start hpajzl_hlava "mlci"
{hodime semka newroom do mapy- objeveni se lokace s obrem}
{a z mapy opet gejt semka...}
  let new_obr (1)
gplend

block _01 ( beg(1) and been(_00) )
title
 labels skip
  justtalk
D: "Herr Zwerg..."
  juststay
  start hpajzl_hlava "mluvi"
P: "Was ist mit meinen Br�dern?"
  start hpajzl_hlava "mlci"
  justtalk
D: "Denen geht's gut."
  goto skip
D: "Sie vergn�gen sich."
 label skip
  juststay
  start hpajzl_hlava "mluvi"
P: "Warum hast du sie nicht hierher gebracht?"
  start hpajzl_hlava "mlci"
  justtalk
D: "Sie sind gerade etwas eingespannt.
    H�tte ich ihnen den Sommerurlaub ruiniert,
    dann w�rd ich mich f�r den Rest meines Lebens
    sch�men."
D: "Das kannst du nicht von mir verlangen."
  juststay
  start hpajzl_hlava "mluvi"
P: "Ach, verschwinde."
  start hpajzl_hlava "mlci"
gplend
